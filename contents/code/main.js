function main() {
  const screens = new Screens();

  workspace.screensChanged.connect(setAvailableScreens.bind(null, screens));
  workspace.windowAdded.connect(monitorNewWindows.bind(null, screens));
}

function setAvailableScreens(screens) {
  screens.setAvailableScreens();
}

function monitorNewWindows(screens, window) {
  if (
    !window.normalWindow ||
    !window.moveable ||
    window.skipTaskbar ||
    window.fullScreen
  ) {
    return;
  }

  if (screens.shouldMoveWindow(window)) {
    // info(JSON.stringify(window, null, 3));
    screens.moveWindow(window);
  }
}

function watchWindow(screens, window) {}

class Config {
  constructor() {
    this.sourceModel = readConfig("sourceScreenModel", "");
    this.targetModel = readConfig("targetScreenModel", "");

    // TODO: config properly
    this.resizeScreenPercent = 0.65;
    this.skipResize = ["firefox"];

    this.validate();
  }

  validate() {
    if (!this.hasConfig())
      throw new Error(
        "Missing config for: 'sourceScreenModel' or 'targetScreenModel'",
      );
  }

  hasConfig() {
    return this.sourceModel && this.targetModel;
  }
}

class Screens {
  constructor(config = undefined) {
    this.config = config || new Config();
    this.availableScreens = undefined;

    this.setAvailableScreens();
    this.logAllScreens();
  }

  setAvailableScreens() {
    this.availableScreens = workspace.screens.reduce(
      (screens, screen, index) => {
        screens[screen.model] = { output: screen, index };

        return screens;
      },
      {},
    );

    return this.hasScreens(true);
  }

  sourceModel() {
    return this.config.sourceModel;
  }

  targetModel() {
    return this.config.targetModel;
  }

  getTargetWindowGeometry(window) {
    const targetArea = workspace.clientArea(
      KWin.MaximizeArea,
      this.getTargetScreen(),
      workspace.currentDesktop,
    );

    const size =
      !this.config.resizeScreenPercent ||
      this.config.skipResize.includes(window.resourceClass)
        ? {}
        : this.calculateWindowSize(targetArea);

    return Object.assign(
      {
        x: targetArea.x,
        y: targetArea.y,
      },
      size,
    );
  }

  calculateWindowSize(targetArea) {
    const width = Math.ceil(targetArea.width * this.config.resizeScreenPercent);
    const height = Math.ceil((targetArea.height / targetArea.width) * width);

    return { width, height };
  }

  shouldMoveWindow(window) {
    return window.output === this.getSourceScreen();
  }

  moveWindow(window) {
    delay(50, () => {
      info(
        `Moving window ${window.resourceClass}(${window.caption}): screen ${
          window.output.model
        } -> ${this.targetModel()}`,
      );
      const targetGeometry = this.getTargetWindowGeometry(window);
      window.frameGeometry.x = targetGeometry.x;
      window.frameGeometry.y = targetGeometry.y;

      if (
        targetGeometry.hasOwnProperty("width") &&
        targetGeometry.hasOwnProperty("height")
      ) {
        window.frameGeometry.width = targetGeometry.width;
        window.frameGeometry.height = targetGeometry.height;
      }
    });
  }

  getSourceScreen() {
    return this.config.sourceModel && this._getScreen(this.config.sourceModel);
  }

  getTargetScreen() {
    return this.config.targetModel && this._getScreen(this.config.targetModel);
  }

  hasScreens(verbose = false) {
    if (!this.availableScreens) {
      throw new Error("No screens found on system");
    }

    return (
      this._hasScreen(this.config.sourceModel, verbose) &&
      this._hasScreen(this.config.targetModel, verbose)
    );
  }

  logAllScreens() {
    Object.entries(this.availableScreens).forEach(([model, screen]) =>
      info(
        `Found screen [${screen.index}]: model: ${model}, manufacturer: ${screen.output.manufacturer}, serial: ${screen.output.serialNumber}, outputName: ${screen.output.name}`,
      ),
    );
  }

  _hasScreen(model, verbose = false) {
    const hasScreen = this.availableScreens.hasOwnProperty(model);
    if (!hasScreen && verbose) warn("Screen not found for model:", model);

    return hasScreen;
  }

  _getScreen(model) {
    if (this._hasScreen(model)) {
      return this.availableScreens[model].output;
    }
  }
}

function delay(milliseconds, callbackFunc) {
  const timer = new QTimer();
  timer.timeout.connect(function () {
    timer.stop();
    callbackFunc();
  });
  timer.start(milliseconds);
  return timer;
}

function debug(msg, ...args) {
  log(msg, "DEBUG", ...args);
}

function info(msg, ...args) {
  log(msg, "INFO", ...args);
}

function warn(msg, ...args) {
  log(msg, "WARN", ...args);
}

function error(msg, ...args) {
  log(msg, "ERROR", ...args);
}

function log(msg, level = "INFO", ...args) {
  print(`KeepWindowsOffScreen[${level}]: ${msg}`, ...args);
}

main();
