NAME = keep-windows-off-screen
PKGFILE = metadata.json

.NOTPARALLEL: all

.PHONY: start stop .log

all: install 

install: $(PKGFILE)
	kpackagetool6 --type KWin/Script --show $(NAME) \
		&& kpackagetool6 --type KWin/Script --upgrade . \
		|| kpackagetool6 --type KWin/Script --install .

uninstall: $(PKGFILE)
	kpackagetool6 --type KWin/Script --remove $(NAME)

refresh: uninstall install

start: stop
	qdbus org.kde.KWin /Scripting org.kde.kwin.Scripting.loadScript '$(NAME)'

stop:
	qdbus org.kde.KWin /Scripting org.kde.kwin.Scripting.unloadScript '$(NAME)'

log:
	journalctl -f QT_CATEGORY=js QT_CATEGORY=kwin_scripting
