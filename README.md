# Keep Windows Off Screen

KWin script to keep new windows off a specified screen by moving them to a specified target screen.

I have a 7'' (17.78cm) touch display to use for showing extra widgets and launching applications, etc. When launching applications by touch, that screen gets focus thus applications would appear on that screen. I made this script to automatically move it to a preferred screen.

See [TODO](#todo) section for shortcomings I'd like to solve.

##### Disclaimer

This is very much a hobby project to learn more about plasma development. I may not be maintaining this project outside solving my needs, unless there is demand. However, I suspect this solves a very niche problem.

### Install


```bash
git clone https://gitlab.com/jeffglover/keep-windows-off-screen.git
cd keep-windows-off-screen
kpackagetool6 --type=KWin/Script -i .
```

### Uninstall

```bash
kpackagetool6 --type=KWin/Script -r keep-windows-off-screen
```

### Usage

1. In Kwin Scripts enable this script
    - While the configuration is required. It needs the "Source Screen Model" and "Target Screen Model" parameters. Which can best be determined by running the script and reading the output logs.
2. Find the model names of you monitor:

```bash
journalctl -f
```

3. Once the script is enabled, look for log messages:

```
KeepWindowsOffScreen[INFO]: Found screen [0]: model: PM320/0, manufacturer: TIM, serial: 0, outputName: DP-5
KeepWindowsOffScreen[INFO]: Found screen [1]: model: S34J55x/1129796695, manufacturer: Samsung Electric Company, serial: 123456, outputName: DP-6
KeepWindowsOffScreen[INFO]: Found screen [2]: model: CL07-HDMI/10000020, manufacturer: Daewoo Electronics Company Ltd, serial: 20000080, outputName: HDMI-A-2
KeepWindowsOffScreen[INFO]: Found screen [3]: model: 0x1603/0, manufacturer: California Institute of Technology, serial: 0, outputName: eDP-1
```

4. Copy the desired model text .e.g, (`PM320/0`, `CL07-HDMI/10000020`) for each of your source and target screens to the configuration
5. Restart the kwin script

### TODO

- Automatically choose the primary screen as the target screen, there does not seem to be any interfaces that would indicate primary.
  - Currently using [`workspace.screens`](https://develop.kde.org/docs/plasma/kwin/api/#read-only-properties-1) which returns a list of screens
  - Look into using `callDBus` assuming screen priority is available.
  - Can the script be written in C++, perhaps that API would provide more system access
- Update the config screen to automatically choose from a list of screens
  - There doesn't seem to be any documentation around `config.ui` outside of use Qt Designer
  - Can I call functions in the `main.js`?
  - build the config screen with C++. Again, this lacks documentation, would likely need to reference other projects
